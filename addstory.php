<?php
session_start();
require 'database.php';

$storytitle = $mysqli->real_escape_string($_POST['storytitle']);
$storycontent = $mysqli->real_escape_string($_POST['storytext']);
$storyuri = $mysqli->real_escape_string($_POST['storyuri']);

$stmt = $mysqli->prepare("insert into stories (username, story_title, story_content, uri) values (?, ?, ?, ?)");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->bind_param('ssss', $_SESSION['username'], $storytitle, $storycontent, $storyuri);
$stmt->execute();
$stmt->close();
header("location:welcome.php");
?>