<!DOCTYPE HTML>

<?php
session_start();
require 'database.php';
$user = $mysqli->real_escape_string($_POST['username']);
if (!empty($user)){

    // Use a prepared statement
    $stmt = $mysqli->prepare("select password_hash FROM users WHERE username=?");

    // Bind the parameter
    $stmt->bind_param('s', $user);
    $stmt->execute();

    // Bind the results
    $stmt->bind_result($pwd_hash);
    $stmt->fetch();

    $pwd_inputted = $mysqli->real_escape_string($_POST['password']);

    // Compare the submitted password to the actual password hash
    if(password_verify($pwd_inputted, $pwd_hash)){
        // Login succeeded!
        $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(32));
        $_SESSION['username'] = $user;
        // Redirect to your target page
        header("Location:welcome.php");
    }else{
        die("Login failed.  Please try again or register.");
        //header("Location:logout.php");
    }
}
?>