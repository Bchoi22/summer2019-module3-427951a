<?php
session_start();
require 'database.php';

$id = $mysqli->real_escape_string($_GET['ID']);
// Use a prepared statement
$stmt = $mysqli->prepare("select story_content from stories where id=?");

// Bind the parameter
$stmt->bind_param('s', $id);
$stmt->execute();

// Bind the results
$stmt->bind_result($theStoryContent);
$stmt->fetch();
$stmt->close();
echo $theStoryContent;
?>