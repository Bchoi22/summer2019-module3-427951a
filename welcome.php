<!DOCTYPE html>


<?php
//$destination_username = $_POST['dest'];
//$amount = $_POST['amount'];
//if(!hash_equals($_SESSION['token'], $_POST['token'])){
//	die("Request forgery detected");
//}
//$mysqli->query(/* perform transfer */);
?>

<head>
<html lang="en-US">
<meta charset="UTF-8">
<title>Welcome page</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
    <body>
        <h1>
            Welcome to a great blog site of world events!
        </h1>
        <h3>Add a story:</h3>
        <form action="addstory.php" method="POST">
            <p>
		        <input type="text" placeholder="Type in the Title of your story" name="storytitle" id="storytitle" size="35"/><br />
	        </p>
	        <p>
		        <input type="text" placeholder="Type or paste the text of your story" name="storytext" id="storytext" size="35" width="20"/>
                OR, <input type="text" placeholder="Type or paste the URI of your story" name="storyuri" id="storyuri" size="35"/>
                <input type="submit" value="Add story" />
	        </p>
        </form><br/><br/>
        <h3>
            Welcome to what's happening in the world from our members!
        </h3>
<?php
session_start();
require 'database.php';
$stmt = $mysqli->prepare("select id, username, story_title, story_content, uri from stories order by username");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->execute();
$result = $stmt->get_result();

echo "<ul>\n";
// Referenced https://www.youtube.com/watch?v=39Kd1UCQu8M for using hrefs icw mysql.
while($row = $result->fetch_assoc()){
    printf("\t<li>username: %s</li>", htmlspecialchars( $row["username"] ));
    $id = htmlspecialchars($row['id']);
    $storyTitle = htmlspecialchars($row['story_title']);
    echo "Link to story: ";
    echo "<a href='viewstory.php?ID=$id'>$storyTitle</a><br />";
    $uri = htmlspecialchars($row['uri']);
    echo "<a href='$uri'>$uri</a><br /><br />";
}
echo "</ul>\n";
$stmt->close();
?><br /><br /><br /><br />

        <form action = "logout.php">
        <input type = 'submit' value = 'Logout'>
        </form>
    </body>
</html>